import { Component } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from './hero.service';
import { OnInit } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
  providers: [HeroService]
})

export class HeroesComponent implements OnInit{
  hero: Hero = { id: 1, name: 'Windstorm'};
  heroes: Hero[];
  selectedHero: Hero;

  constructor(private heroService: HeroService, private router: Router){

  }

  ngOnInit(){
    this.getHeroes();
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  getHeroes(): void{
    this.heroService.getHeroes().then(heroes => this.heroes = heroes);
  }

  goToDetail() : void{
    this.router.navigate(['/detail', this.selectedHero.id]);
  }
}


  
